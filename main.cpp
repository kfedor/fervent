#include "fervent.h"

#include <thread>
#include <iostream>
#include <chrono>
#include <signal.h>

Fervent fev;

void event_handler( void *args )
{
	std::cout << "I'm sync handler with args " << std::hex << args << std::endl;
}

void event_thread_handler( void *args )
{
	std::cout << "I'm sync thread handler with args " << std::hex << args << std::endl;
}

void timed_handler( void *args )
{
	std::cout << "I'm timed handler 1000 ms lol " << std::hex << args << std::endl;
}

void timed_handler2( void *args )
{
	std::cout << "I'm timed handler 100ms " << std::hex << args << std::endl;
}


void thread_handler()
{
	for( int i=0; i<10; i++ )
	{
		fev.addEvent( event_thread_handler, ( void* )i );
		std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
	}

	fev.stop();
}

void signal_callback_handler( int signum )
{
	std::cout << "Caught signal\n";
	fev.stop();
}

int main()
{
	signal( SIGINT, signal_callback_handler );
	signal( SIGTERM, signal_callback_handler );

	//fev.addEvent( event_handler, NULL );
	//fev.addEvent( event_handler, NULL );
	//fev.addEvent( event_handler, NULL );

	fev.addTimer( timed_handler, NULL, 10000 );
	fev.addTimer( timed_handler2, NULL, 10000 );
/*
	for( int i=0; i<10; i++ )
	{
		std::thread thr_handle = std::thread( thread_handler );
		thr_handle.detach();
	}
*/
	std::cout << "Starting loop\n";
	fev.loop();


	return 0;
}
